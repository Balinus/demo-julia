using ClimateTools, Glob, PyPlot, AxisArrays

# ======================
# PARAMETERS
# ======================
repfig = "/home/proy/GitRepos/demo-julia/Figures/"
# polygones
poly_gcm = [[NaN -62 -75 -75 -62 -62];[NaN 40 40 50 50 40]]
poly_rcm = [[NaN -63 -75.5 -75.5 -63 -63];[NaN 43 43 49.5 49.5 43]]
poly_obs = [[NaN -64 -75 -75 -64 -64];[NaN 42 42 49.5 49.5 42]]

cmin = 2.2
cmax = 5.5

# REFERENCE
repData = "/home/proy/SCEN3_root/scen2/scenario/migration_netcdf/nrcan/nrcan_canada_daily/"
nrcan_pr = glob("nrcan*pr*", repData)
nrcan_pr = nrcan_pr[12:21] # do only 1961-1990

# ECHAM5
prefixECHAM5_histo = "/home/proy/SCEN3_root/dmf2/scenario/external_data/CMIP5/MPI-M/MPI-ESM-LR/historical/day/atmos/r1i1p1/pr/"
filesECHAM5_pr_histo = [string(prefixECHAM5_histo, "pr_day_MPI-ESM-LR_historical_r1i1p1_19600101-19691231.nc"), string(prefixECHAM5_histo, "pr_day_MPI-ESM-LR_historical_r1i1p1_19700101-19791231.nc"), string(prefixECHAM5_histo, "pr_day_MPI-ESM-LR_historical_r1i1p1_19800101-19891231.nc"), string(prefixECHAM5_histo, "pr_day_MPI-ESM-LR_historical_r1i1p1_19900101-19991231.nc")]

# CORDEX MRCC5-MPI
repCordex_pr = "/home/proy/SCEN3_root/dmf2/scenario/external_data/CORDEX/UQAM/CRCM5/NAM-44_MPI-M-MPI-ESM-LR_historical/day/atmos/r1i1p1/pr/"
repCordex_CanRCM4_pr = "/home/proy/SCEN3_root/dmf2/scenario/external_data/CORDEX/CCCMA/CanRCM4/NAM-44_CCCma-CanESM2_historical/day/atmos/r1i1p1/pr/"
repCordex_RCA4_pr = "/home/proy/SCEN3_root/dmf2/scenario/external_data/CORDEX/SMHI/RCA4/NAM-44_CCCma-CanESM2_historical/day/atmos/r1i1p1/pr"
filesCordex_pr = glob("*.nc", repCordex_pr)
filesCordex_pr = filesCordex_pr[4:5]


# ===============================
# LOAD DATA
# ===============================
ECHAM5 = ClimateTools.load(filesECHAM5_pr_histo, "pr", poly=poly_gcm, data_units="mm")
# ensure temporal subset
ECHAM5 = temporalsubset(ECHAM5, (1961, 01, 01), (1970, 12, 31))

MRCC5_pr_histo = ClimateTools.load(filesCordex_pr, "pr", poly=poly_rcm, data_units="mm")
obs_pr = ClimateTools.load(nrcan_pr, "pr", poly=poly_obs, data_units="mm")

# ======================
# REGRID (INTERPOLATIONS)
# ======================
ECHAM5_regrid = regrid(ECHAM5, obs_pr)
MRCC5_pr_histo_regrid = regrid(MRCC5_pr_histo, obs_pr)
p = Progress(length(timeorig), 5, "Regridding: ")

# =========================
# QUANTILE-QUANTILE MAPPING
# =========================
MRCC5_pr_histo_regrid_qqmap = qqmap(obs_pr, MRCC5_pr_histo_regrid, MRCC5_pr_histo_regrid, method="multiplicative", detrend=false);
ECHAM5_regrid_qqmap = qqmap(obs_pr, ECHAM5_regrid, ECHAM5_regrid, detrend=false, method="multiplicative")

# ===============
# MAPS
# ===============

mapclimgrid(ECHAM5, region="south_quebec", cs_label="mm/jour", ncolors=4, caxis=[cmin, cmax], filename=repfig*"MPI_histo_pr_raw.png")
PyPlot.close()
mapclimgrid(MRCC5_pr_histo, region="south_quebec", cs_label="mm/jour", ncolors=11, caxis=[cmin, cmax], filename=repfig*"MRCC5-MPI_histo_pr_raw.png")
PyPlot.close()
mapclimgrid(obs_pr, region="south_quebec", cs_label="mm/jour", ncolors=6, caxis=[cmin, cmax], titlestr="Observations 1961-1990", filename=repfig*"OBS_pr_1961-1990.png")
PyPlot.close()
mapclimgrid(MRCC5_pr_histo_regrid, region="south_quebec", cs_label="mm/jour", titlestr="MRCC5-MPI-REGRID", ncolors=11, caxis=[cmin, cmax], filename=repfig*"MRCC5_MPI_histo_pr_regrid.png")
PyPlot.close()

mapclimgrid(ECHAM5_regrid, region="south_quebec", cs_label="mm/jour", ncolors=4, caxis=[cmin, cmax], titlestr="MPI-ESM-LR-REGRID", filename=repfig*"MPI_histo_pr_regrid.png")
PyPlot.close()

mapclimgrid(MRCC5_pr_histo_regrid_qqmap, region="south_quebec", cs_label="mm/jour", ncolors=6, caxis=[cmin, cmax], titlestr="MRCC5-REGRID-QQMAP 1961-1990", filename=repfig*"MRCC5_MPI_histo_1961-1990_pr_regrid_qqmap.png")
PyPlot.close()
mapclimgrid(ECHAM5_regrid_qqmap, region="south_quebec", cs_label="mm/jour", titlestr="MPI-ESM-LR-REGRID-QQMAP 1961-1990", ncolors=6, caxis=[cmin, cmax], filename=repfig*"MPI_histo_1961-1990_pr_regrid_qqmap.png")
PyPlot.close()

# ==================
# DIFF BETWEEN INTERP AND QQMAP
mapclimgrid(periodmean(MRCC5_pr_histo_regrid_qqmap) - periodmean(MRCC5_pr_histo_regrid), region="south_quebec", cs_label="mm/jour", titlestr="MRCC5 - QQMAP moins REGRID 1961-1990", filename=repfig*"MRCC5_histo_1961-1990_pr_regrid_qqmap_diff.png")
PyPlot.close()

mapclimgrid(periodmean(ECHAM5_regrid_qqmap) - periodmean(ECHAM5_regrid), region="south_quebec", cs_label="mm/jour", titlestr="MPI - QQMAP moins REGRID 1961-1990",  filename=repfig*"MPI_histo_1961-1990_pr_regrid_qqmap_diff.png")
PyPlot.close()

mapclimgrid(periodmean(ECHAM5_regrid_qqmap) - periodmean(MRCC5_pr_histo_regrid_qqmap), region="south_quebec", cs_label="mm", titlestr="MPI moins MRCC5 DIFFERENCE", filename=repfig*"MRCC5-MPI_histo_1961-1990_pr_regrid_qqmap_diff.png")
PyPlot.close()

mapclimgrid(annualmax(ECHAM5_regrid_qqmap) - annualmax(MRCC5_pr_histo_regrid_qqmap), region="south_quebec", cs_label="mm", titlestr="MPI moins MRCC5 - Annualmax DIFFERENCE", filename=repfig*"MRCC5-MPI_histo_1961-1990_pr_regrid_qqmap_annualmax_diff.png")
PyPlot.close()

# mapclimgrid(periodmean(annualmax(ECHAM5_regrid_qqmap)) / periodmean(annualmax(obs_pr)), region="south_quebec", cs_label="mm", titlestr="MPI / OBS - Annualmax RATIO", filename=repfig*"MPI_vs_OBS_histo_1961-1990_pr_regrid_qqmap_annualmax_ratio_periodmean.png")
# PyPlot.close()

mapclimgrid(annualmax(MRCC5_pr_histo_regrid_qqmap) / annualmax(obs_pr), region="south_quebec", cs_label="mm", titlestr="MRCC5 / OBS - Annualmax RATIO", filename=repfig*"MRCC5_vs_OBS_histo_1961-1990_pr_regrid_qqmap_annualmax_ratio.png")
PyPlot.close()


#
plot(annualmax(obs_pr), label="OBS")
plot(annualmax(ECHAM5), label="MPI - ORIG")
plot(annualmax(ECHAM5_regrid_qqmap), label="MPI - QQMAP")
plot(annualmax(MRCC5_pr_histo), label="MRCC5 - ORIG")
plot(annualmax(MRCC5_pr_histo_regrid_qqmap), label="MRCC5 - QQMAP")
