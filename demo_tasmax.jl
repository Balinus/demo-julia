using ClimateTools, Glob, PyPlot, AxisArrays
cd("/home/proy/GitRepos/demo-julia/Figures/")
# polygones
poly_gcm = [[NaN -62 -75 -75 -62 -62];[NaN 40 40 50 50 40]]
poly_rcm = [[NaN -63 -75.5 -75.5 -63 -63];[NaN 43 43 49.5 49.5 43]]
poly_obs = [[NaN -64 -75 -75 -64 -64];[NaN 42 42 49.5 49.5 42]]

# REFERENCE
repData = "/home/proy/SCEN3_root/scen2/scenario/migration_netcdf/nrcan/nrcan_canada_daily/"
nrcan_tasmax = glob("nrcan*tasmax*", repData)
nrcan_tasmax = nrcan_tasmax[12:41] # do only 1961-1990

# ============================
# ============================
# TEMPERATURE
# =============================

# ECHAM5
prefixECHAM5_histo = "/home/proy/SCEN3_root/dmf2/scenario/external_data/CMIP5/MPI-M/MPI-ESM-LR/historical/day/atmos/r1i1p1/tasmax/"
filesECHAM5_tasmax_histo = [string(prefixECHAM5_histo, "tasmax_day_MPI-ESM-LR_historical_r1i1p1_19600101-19691231.nc"), string(prefixECHAM5_histo, "tasmax_day_MPI-ESM-LR_historical_r1i1p1_19700101-19791231.nc"), string(prefixECHAM5_histo, "tasmax_day_MPI-ESM-LR_historical_r1i1p1_19800101-19891231.nc"), string(prefixECHAM5_histo, "tasmax_day_MPI-ESM-LR_historical_r1i1p1_19900101-19991231.nc")]

# CORDEX MRCC5-MPI
repCordex_tasmax = "/home/proy/SCEN3_root/dmf2/scenario/external_data/CORDEX/UQAM/CRCM5/NAM-44_MPI-M-MPI-ESM-LR_historical/day/atmos/r1i1p1/tasmax/"
repCordex_tasmax_fut = "/home/proy/SCEN3_root/dmf2/scenario/external_data/CORDEX/UQAM/CRCM5/NAM-44_MPI-M-MPI-ESM-LR_rcp85/day/atmos/r1i1p1/tasmax/"

filesCordex_tasmax = glob("*.nc", repCordex_tasmax)
filesCordex_tasmax = filesCordex_tasmax[4:9]

filesCordex_tasmax_fut = glob("*.nc", repCordex_tasmax_fut)
filesCordex_tasmax_fut = filesCordex_tasmax_fut[end-5:end]


# ===============================
# DATA - TASMAX
cmin = 1.85#275.0 - 273.15
cmax = 14.85#288.0 - 273.15

# MPI
ECHAM5_tasmax_histo = load(filesECHAM5_tasmax_histo, "tasmax", poly=poly_gcm, data_units="Celsius")
ECHAM5_tasmax_histo = temporalsubset(ECHAM5_tasmax_histo, (1961, 01, 01), (1990, 12, 31))

# MRCC5
MRCC5_tasmax_histo = ClimateTools.load(filesCordex_tasmax, "tasmax", poly=poly_rcm, data_units="Celsius")
MRCC5_tasmax_fut = ClimateTools.load(filesCordex_tasmax_fut, "tasmax", poly=poly_rcm, data_units="Celsius")

# NRCAN
obs_tasmax = ClimateTools.load(nrcan_tasmax, "tasmax", poly=poly_obs, data_units="Celsius")

# ================
# REGRID
MRCC5_tasmax_histo_regrid = regrid(MRCC5_tasmax_histo, obs_tasmax)
MRCC5_tasmax_fut_regrid = regrid(MRCC5_tasmax_fut, obs_tasmax)
ECHAM5_regrid = regrid(ECHAM5_tasmax_histo, obs_tasmax)
# Random matrix
datarnd = randn(size(obs_tasmax[1],1), size(obs_tasmax[1],2), size(obs_tasmax[1],3));
axisdata = AxisArray(datarnd, Axis{:lon}(obs_tasmax[1][Axis{:lon}][:]), Axis{:lat}(obs_tasmax[1][Axis{:lat}][:]), Axis{:time}(obs_tasmax[1][Axis{:time}][:]))

Crnd = ClimGrid(axisdata, longrid=obs_tasmax.longrid, latgrid=obs_tasmax.latgrid, msk=obs_tasmax.msk, grid_mapping=obs_tasmax.grid_mapping, dimension_dict=obs_tasmax.dimension_dict, model="Random", frequency=obs_tasmax.frequency, experiment="Random", run="Random", project="Random", institute="Phil's institute", filename="Random generation", dataunits=obs_tasmax.dataunits, latunits=obs_tasmax.latunits, lonunits=obs_tasmax.lonunits, variable=obs_tasmax.variable, typeofvar=obs_tasmax.typeofvar, typeofcal=obs_tasmax.typeofcal, varattribs=obs_tasmax.varattribs, globalattribs=obs_tasmax.globalattribs)

GC.gc()

# ==============
# QQMAP
MRCC5_tasmax_histo_regrid_qqmap = qqmap(obs_tasmax, MRCC5_tasmax_histo_regrid, MRCC5_tasmax_histo_regrid, detrend=false)
MRCC5_tasmax_fut_regrid_qqmap = qqmap(obs_tasmax, MRCC5_tasmax_histo_regrid, MRCC5_tasmax_fut_regrid, detrend=true)
# MRCC5_tasmax_histo_regrid = 0; GC.gc()
ECHAM5_regrid_qqmap = qqmap(obs_tasmax, ECHAM5_regrid, ECHAM5_regrid, detrend=false)

Crnd_qqmap = qqmap(obs_tasmax, Crnd, Crnd, detrend=false)
# ECHAM5_regrid = 0; GC.gc()

################
# FIGURES

mapclimgrid(ECHAM5_tasmax_histo, region="south_quebec", cs_label="Celsius", titlestr="MPI-ESM-LR-1961-1990", ncolors=8, caxis=[cmin, cmax], filename="MPI_histo_tasmax.png")
PyPlot.close()

mapclimgrid(MRCC5_tasmax_histo, region="south_quebec", cs_label="Celsius", titlestr="OURANOS-CRCM5-1961-1990", ncolors=9, caxis=[cmin, cmax], filename="MRCC5_histo_tasmax.png")
PyPlot.close()

mapclimgrid(obs_tasmax, region = "south_quebec", cs_label="Celsius", titlestr="Observations 1961-1990", ncolors=12, caxis=[cmin, cmax], filename="OBS_tasmax_1961-1990.png")
PyPlot.close()

mapclimgrid(Crnd, region = "south_quebec", cs_label="Celsius", titlestr="Random", ncolors=12, center_cs=true, filename="Random_tasmax_1961-1990.png")
PyPlot.close()

mapclimgrid(MRCC5_tasmax_histo_regrid, region="south_Quebec", cs_label="Celsius", titlestr="OURANOS-CRCM5-REGRID", ncolors=9, caxis=[cmin, cmax],filename="MRCC5_histo_tasmax_regrid.png")
PyPlot.close()


mapclimgrid(ECHAM5_regrid, region="south_Quebec", cs_label="Celsius", titlestr="MPI-ESM-LR-REGRID", ncolors=9, caxis=[cmin, cmax], filename="MPI_histo_tasmax_regrid.png")
PyPlot.close()

mapclimgrid(MRCC5_tasmax_histo_regrid_qqmap, region = "south_Quebec", cs_label="Celsius", titlestr="OURANOS-CRCM5-REGRID-QQMAP", ncolors=12, caxis=[cmin, cmax], filename="MRCC5_histo_1961-1990_tasmax_regrid_qqmap.png")
PyPlot.close()

mapclimgrid(ECHAM5_regrid_qqmap, region = "south_Quebec", cs_label="Celsius", titlestr="MPI-ESM-LR-REGRID-QQMAP", ncolors=12, caxis=[cmin, cmax], filename="MPI_histo_1961-1990_tasmax_regrid_qqmap.png")
PyPlot.close()

mapclimgrid(Crnd_qqmap, region = "south_Quebec", cs_label="Celsius", titlestr="Random-QQMAP", ncolors=12, caxis=[cmin, cmax], filename="Random_tasmax_regrid_qqmap.png")
PyPlot.close()

# ==================
# # DIFF BETWEEN INTERP AND QQMAP
# mapclimgrid(MRCC5_tasmax_histo_regrid_qqmap-MRCC5_tasmax_histo_regrid, region = "Quebec", cs_label="mm/jour", titlestr="OURANOS-CRCM5-REGRID-QQMAP DIFF 1961-1990", filename="MRCC5_histo_1961-1990_tasmax_regrid_qqmap_diff.png")

# mapclimgrid(ECHAM5_tasmax_histo_regrid_qqmap-ECHAM5_tasmax_histo_regrid, region = "Quebec", cs_label="Kelvin", titlestr="OURANOS-CRCM5-REGRID-QQMAP 1961-1970", filename="MPI_histo_1961-1970_tasmax_regrid_qqmap_diff.png")

mapclimgrid(ECHAM5_regrid_qqmap - MRCC5_tasmax_histo_regrid_qqmap, region = "south_quebec", cs_label="Celsius", titlestr="OURANOS-MPI DIFFERENCE", center_cs=true, filename="MRCC5-MPI_histo_1961-1990_tasmax_regrid_qqmap_diff.png")
PyPlot.close()

# mapclimgrid(periodmean(ECHAM5_regrid_qqmap) - periodmean(MRCC5_tasmax_histo_regrid_qqmap), region = "Quebec", cs_label="Kelvin", titlestr="OURANOS-MPI DIFFERENCE", filename="MRCC5-MPI_histo_1961-1990_tasmax_regrid_qqmap_diff2.png")
# PyPlot.close()

mapclimgrid(annualmax(ECHAM5_regrid_qqmap) - annualmax(MRCC5_tasmax_histo_regrid_qqmap), region = "south_quebec", cs_label="Celsius", titlestr="OURANOS-MPI DIFFERENCE - Max annuels", cm="Blues_r", filename="MRCC5-MPI_histo_1961-1990_tasmax_regrid_qqmap_annualmax_diff.png")
PyPlot.close()

# plot(annualmax(obs_tasmax), label="Observations")
# plot(annualmax(ECHAM5_regrid_qqmap), label="MPI - QQMAP")
# plot(annualmax(MRCC5_tasmax_histo_regrid_qqmap), label="MRCC5 - QQMAP", titlestr="Maximum annuel")

# using DataFrames
# df = DataFrame(obs=obs_tasmax[1][:], b=ECHAM5_regrid_qqmap[1][:], c=MRCC5_tasmax_histo_regrid_qqmap[1][:])


plot(annualmax(obs_tasmax), label="OBS")
plot(annualmax(ECHAM5_tasmax_histo), label="MPI - ORIG")
plot(annualmax(ECHAM5_regrid_qqmap), label="MPI - QQMAP")
plot(annualmax(MRCC5_tasmax_histo), label="MRCC5 - ORIG")
plot(annualmax(MRCC5_tasmax_histo_regrid_qqmap), label="MRCC5 - QQMAP", titlestr="Maximums annuels", ylimit=[18, 36])


# plot(annualmax(ECHAM5_tasmax_histo) - annualmax(obs_tasmax), label="DIFF - ORIG")



# # BCG simulation
# prefixbcg = "/home/proy/SCEN3_root/expl6/climato/arch/bcg/series/"
# suffixbcg = "_bcg_"

# years = 1961:1990
# months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
# filesMRCC5_pr_histo = Array{String}(undef, length(years)*12)
# filesMRCC5_tasmax_histo = Array{String}(undef, length(years)*12)

# global z = 1
# for iyear in years
#     for imonth in months
#         filesMRCC5_pr_histo[z] = string(prefixbcg, iyear, imonth, "/", "pr", suffixbcg, iyear, imonth, "_se.nc")
#         filesMRCC5_tasmax_histo[z] = string(prefixbcg, iyear, imonth, "/", "tasmax", suffixbcg, iyear, imonth, "_se.nc")
#         z += 1

#     end
# end

# # BCW simulation
# prefixbcw = "/home/proy/SCEN3_root/expl7/climato/arch/bcw/series/"
# suffixbcw = "_bcw_"

# years = 2041:2070
# months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
# filesMRCC5_pr_fut = Array{String}(undef, length(years)*12)
# filesMRCC5_tasmax_fut = Array{String}(undef, length(years)*12)

# global z = 1
# for iyear in years
#     for imonth in months
#         filesMRCC5_pr_fut = string(prefixbcw, iyear, imonth, "/", "pr", suffixbcw, iyear, imonth, "_se.nc")
#         filesMRCC5_tasmax_fut = string(prefixbcw, iyear, imonth, "/", "tasmax", suffixbcw, iyear, imonth, "_se.nc")
#         global z += 1

#     end
# end