using AxisArrays
using Dates

# Data
data = rand(366, 3)

# Dimensions
datevec = DateTime(2000, 01, 01):Day(1):DateTime(2000, 12, 31)
col1 = :Tmax
col2 = :Tmin
col3 = :Precip

# Creation AxisArray
A = AxisArray(data, Axis{:time}(datevec), Axis{:variable}([col1, col2, col3]))


# Fonction par défaut, sans spécification de type
function foo(a, b) # version 1
    # Cette fonction peut être utilisée pour tous les types
    # qui implémentent l'opérateur +
    return a + b
end

function foo(a::Integer, b) # version 2
    println("Le premier argument est un entier")
    return a + b
end

function foo(a, b::Integer) # version 3
    println("Le second argument est un entier")
    return a + b
end

function foo(a::Integer, b::Integer) # version 4
    println("Les deux arguments sont des entier")
    return a + b
end

function foo(a::String, b::Integer) # version 5
    println("Le premier argument est une chaîne ",
            "et le second argument est un entier")
    return string(a, b)
end

foo(3.6, 5.9)      # utilise la version 1
foo(3, 5.9)        # utilise la version 2
foo(3.6, 6)        # utilise la version 3
foo(4, 2)          # utilise la version 4
foo("Bonjour ", 5) # utilise la version 5


function foo(x::T where T)
    return x^2
end

using Statistics
function bar(x, q)
    return quantile(x, q)
end

import Base: +
# Type "custom"
struct Point
    x::Float64
    y::Float64
end

# Fonction sur ces types
function Base.:+(p::Point, q::Point)
    return Point(p.x + q.x, p.y + q.y)
end


# Paquets
using Distributions: Beta
using StatsPlots

# Paramètres
a = 1.0
b = 4.0
n = 1000
# Sampling
data = rand(Beta(a, b), n)

# Figure
histogram(data, normalize=true, label="Sampled") # DATA BRUT
plot!(Beta(a, b), linewidth=6, label="Theoretical") # Distribution théorique
