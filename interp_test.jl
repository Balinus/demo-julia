using ClimateTools
using BenchmarkTools
using Glob
using AxisArrays
using Dates
using NCDatasets
using Interpolations
using ArgCheck

poly_obs = [[NaN -75.5 -76 -76 -75.5 -75.5];[NaN 45 45 45.2 45.2 45]]
poly_gcm = [[NaN -60 -92 -92 -60 -60];[NaN 32 32 52 52 32]]

canesm2_fut = "/home/proy/NEREE_root/dmf2/scenario/external_data/CMIP5/CCCMA/CanESM2/rcp85/day/atmos/r3i1p1/pr/pr_day_CanESM2_rcp85_r3i1p1_20060101-21001231.nc"

# poly_gcm = [[NaN -56 -82 -82 -56 -56];[NaN 40 40 65 65 40]]

A = load(canesm2_fut, "pr", poly=poly_gcm, start_date=(2041, 01, 01), end_date=(2041, 01, 31, 23))

repData = "/home/proy/SCEN3_root/scen2/scenario/migration_netcdf/nrcan/nrcan_canada_daily/"
nrcan_pr = glob("nrcan*pr*", repData)
# nrcan_pr = nrcan_pr[12:41] # 30 ans
nrcan_pr = nrcan_pr[1] # 30 ans

B = load(nrcan_pr, "pr", poly=poly_obs)

# Regrid
interp = regrid(A, B)



# BenchmarkTools.Trial:
#   memory estimate:  49.38 MiB
#   allocs estimate:  41999
#   --------------
#   minimum time:     100.284 ms (3.37% GC)
#   median time:      107.897 ms (3.47% GC)
#   mean time:        113.736 ms (5.87% GC)
#   maximum time:     210.518 ms (50.38% GC)
#   --------------
#   samples:          44
#   evals/sample:     1

  # WITH DROPFEB29

  # BenchmarkTools.Trial:
  # memory estimate:  40.54 MiB
  # allocs estimate:  40806
  # --------------
  # minimum time:     77.059 ms (0.00% GC)
  # median time:      78.078 ms (0.00% GC)
  # mean time:        80.001 ms (0.00% GC)
  # maximum time:     110.188 ms (0.00% GC)
  # --------------
  # samples:          63
  # evals/sample:     1

# WITH DROPFEB29 & REMOVED THREADS IN POLYFIT AND POLYVAL
# BenchmarkTools.Trial:
#   memory estimate:  28.56 MiB
#   allocs estimate:  39552
#   --------------
#   minimum time:     68.877 ms (2.49% GC)
#   median time:      71.037 ms (2.61% GC)
#   mean time:        77.866 ms (3.08% GC)
#   maximum time:     143.005 ms (1.25% GC)
#   --------------
#   samples:          65
#   evals/sample:     1



# ====================
# Single vector

days = 1:365
d = collect(DateTimeNoLeap(1961,01,01):Day(1):DateTimeNoLeap(1990, 12, 31))
obsvec = rand(length(d))
refvec = rand(length(d))
futvec = rand(length(d))

datevec_obs = dayofyear.(d)
datevec_ref = dayofyear.(d)
datevec_fut = dayofyear.(d)

#80ms (single-thread)
@benchmark qqmap($obsvec, $refvec, $futvec, $days, $datevec_obs, $datevec_ref, $datevec_fut)

# # SINGLE VECTOR ORIGINAL
# BenchmarkTools.Trial:
#   memory estimate:  31.82 MiB
#   allocs estimate:  22268
#   --------------
#   minimum time:     80.624 ms (2.11% GC)
#   median time:      81.666 ms (2.26% GC)
#   mean time:        82.193 ms (3.18% GC)
#   maximum time:     84.629 ms (4.97% GC)
#   --------------
#   samples:          61
#   evals/sample:     1

# WITH DROPFEB29
# BenchmarkTools.Trial:
#   memory estimate:  22.29 MiB
#   allocs estimate:  20810
#   --------------
#   minimum time:     64.897 ms (2.68% GC)
#   median time:      65.054 ms (2.78% GC)
#   mean time:        65.156 ms (2.85% GC)
#   maximum time:     67.173 ms (5.62% GC)
#   --------------
#   samples:          77
#   evals/sample:     1



 # qqmap(obsvec::Array{N,1} where N, refvec::Array{N,1} where N, futvec::Array{N,1} where N, days, datevec_obs, datevec_ref, datevec_fut; method::String="Additive", detrend::Bool=true, window::Int64=15, rankn::Int64=50, thresnan::Float64=0.1, keep_original::Bool=false, interp=Linear(), extrap=Flat())
