using ClimateTools
using CSV
using Glob
# using Distances
using Geodesy
using MAT
using Statistics
using Extremes
using Distributions

# Utilities
include("/home/proy/GitRepos/demo-julia/utils.jl")


# GEV parameters
file_params = "/home/proy/GitRepos/demo-julia/data/GEV_24h_pcp_interpolated_v2.csv"
df_params = CSV.read(file_params)
#
# # Polygones
# poly_gcm = [[NaN -70 -75 -75 -70 -70];[NaN 42 42 48 48 42]]
# poly_rcm = [[NaN -63 -75.5 -75.5 -63 -63];[NaN 43 43 49.5 49.5 43]]
# poly_obs = [[NaN -73.5 -74 -74 -73.5 -73.5];[NaN 45.5 45.5 46 46 45.5]]
#
# # Obs data
# repData = "/home/proy/SCEN3_root/scen2/scenario/migration_netcdf/nrcan/nrcan_canada_daily/"
# nrcan_pr = glob("nrcan*pr*", repData)
# nrcan_pr = nrcan_pr[12:41]
#
#
# # Ref
# prefixECHAM5_histo = "/home/proy/SCEN3_root/dmf2/scenario/external_data/CMIP5/MPI-M/MPI-ESM-LR/historical/day/atmos/r1i1p1/pr/"
# filesECHAM5_pr_histo = [joinpath(prefixECHAM5_histo, "pr_day_MPI-ESM-LR_historical_r1i1p1_19600101-19691231.nc"), joinpath(prefixECHAM5_histo, "pr_day_MPI-ESM-LR_historical_r1i1p1_19700101-19791231.nc"), joinpath(prefixECHAM5_histo, "pr_day_MPI-ESM-LR_historical_r1i1p1_19800101-19891231.nc"), joinpath(prefixECHAM5_histo, "pr_day_MPI-ESM-LR_historical_r1i1p1_19900101-19991231.nc")]
#
# # Fut data
# prefixECHAM5_fut = "/home/proy/SCEN3_root/dmf2/scenario/external_data/CMIP5/MPI-M/MPI-ESM-LR/rcp85/day/atmos/r1i1p1/pr"
# filesECHAM5_pr_fut = [joinpath(prefixECHAM5_fut, "pr_day_MPI-ESM-LR_rcp85_r1i1p1_20060101-20091231.nc"), joinpath(prefixECHAM5_fut, "pr_day_MPI-ESM-LR_rcp85_r1i1p1_20100101-20191231.nc"), joinpath(prefixECHAM5_fut, "pr_day_MPI-ESM-LR_rcp85_r1i1p1_20200101-20291231.nc"), joinpath(prefixECHAM5_fut, "pr_day_MPI-ESM-LR_rcp85_r1i1p1_20300101-20391231.nc")]
#
# # Extract data
# obs = load(nrcan_pr, "pr", poly=poly_obs, data_units="mm")
# ref = load(filesECHAM5_pr_histo, "pr", poly=poly_gcm, data_units="mm")
# fut = load(filesECHAM5_pr_fut, "pr", poly=poly_gcm, data_units="mm")
#
# # Temporal subset of ref
# ref = temporalsubset(ref, (1961, 01, 01), (1990, 12, 31, 23))
#
# # Modify dates (e.g. 29th feb are dropped/lost by default)
# obs = ClimateTools.dropfeb29(obs)
# ref = ClimateTools.dropfeb29(ref)
# fut = ClimateTools.dropfeb29(fut)
#
# # Regrid
# ref = regrid(ref, obs)
# fut = regrid(fut, obs)
#
# lon_params = df_params[:lon]
# lat_params = df_params[:lat]
#
# lon_model, lat_model = ClimateTools.getgrids(ref)
#
# points_params = build_params_coords(lon_params, lat_params)

obsmat = matread("/home/proy/GitRepos/demo-julia/data/obs.mat")
refmat = matread("/home/proy/GitRepos/demo-julia/data/ref.mat")
futmat = matread("/home/proy/GitRepos/demo-julia/data/fut.mat")

obs = obsmat["obs"]
obs_jul = obsmat["obs_jul"]

ref = refmat["ref"]
ref_jul = refmat["ref_jul"]

fut = futmat["fut"]
fut_jul = futmat["fut_jul"]

μ = df_params[1,:mu]
σ = df_params[1,:sigma]
ξ = df_params[1,:xi]

P = 0.95 # threshold for POT

# loop over grid points
# extract GEV parameters
# Do bias-correction
for j = 1:size(fut[1], 2)
    for i = 1:size(fut[1], 1)
        obsvec = obs[1][i, j, :]
        refvec = ref[1][i, j, :]
        futvec = fut[1][i, j, :]





    end
end

function biascorrect_extremes(obs, ref, fut; P::Real=0.95, μ::Real, σ::Real, ξ::Real)

    # Do a proper quantile-quantile mapping
    qqmapvec = qqmap(obs, ref, fut, etc...)

    seuil = mean([quantile(obs[obs .>= 1.0], P) quantile(ref[ref .>= 1.0], P)])

    # Extract clusters
    obs_cluster = getcluster(obs, seuil)
    ref_cluster = getcluster(ref, seuil)
    fut_cluster = getcluster(fut, seuil)

    # Fit Generalized Pareto Distributions
    obs_gpd_hard = GeneralizedPareto(μ, σ, ξ)
    obs_gpd = gpdfit(obs_cluster[:Max], threshold=seuil)
    ref_gpd = gpdfit(ref_cluster[:Max], threshold=seuil)
    fut_gpd = gpdfit(fut_cluster[:Max], threshold=seuil)

    # Estimates empirical CDF
    cdf_obs = cdf.(obs_gpd, obs_cluster[:Max])
    cdf_ref = cdf.(ref_gpd, ref_cluster[:Max])
    cdf_fut = cdf.(fut_gpd, fut_cluster[:Max])

    newfut = quantile.(obs_gpd, cdf_fut)
    newfut_hard = quantile.(obs_gpd_hard, cdf_fut)

    # Transition
    # TODO transition


end
