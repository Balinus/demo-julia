using Makie, GeoMakie, Proj4, MakieLayout
using ClimateTools
using AxisArrays


filename = "/media/DATA1TB/CMIP5/pr_day_CanESM2_rcp85_r1i1p1_20060101-21001231.nc"
C = load(filename, "pr", start_date=(2080,01,01), end_date=(2080, 03, 31, 23))
A = C.data.data
# lons = C[1][Axis{:lon}]

lons, lats, timevec = ClimateTools.getdims(C)

colorfunc(i) = circshift(A[:, :, i], 180)
# Amean = circshift(dropdims(mean(A[:, :, :], dims=3), dims=3), 180) # average `A` over 19 years
Amean = dropdims(mean(A[:, :, :], dims=3), dims=3)
texttime(i) = string(C.model, " - ", year(timevec[i]), " - ", month(timevec[i]), " - ", day(timevec[i]))
titletext = C.model#"$(attrib["long_name"]), units=[$units], average of 19 years"

projection = "moll"
cmap = :deep

source = Projection("+proj=lonlat +lon_0=0")
dest = Projection("+proj=$projection +lon_0=0")

# source = LonLat()
# dest = WinkelTripel()

xs, ys = xygrid(lons, lats)
Proj4.transform!(source, dest, vec(xs), vec(ys))

# get aspect ratio
xmin, xmax = extrema(xs)
ymin, ymax = extrema(ys)
aspect_ratio = (ymax - ymin) / (xmax - xmin)

total_crange = (minimum(A), maximum(A))
# total_crange = (0, 400)

function plotfield(Amean, crange = (minimum(Amean), maximum(Amean));
                   cmap = :viridis, titletext)
    scene, layout = layoutscene(40; resolution = (900, 475));
    # cmap = to_colormap(:viridis, 100)
    earthscene = layout[1, 1] = LScene(scene);

    sf = surface!(earthscene, xs, ys, zeros(size(xs)); color = Amean,
            shading = false, show_axis = false, colorrange = crange, colormap = cmap);

    geoaxis!(earthscene, -180, 180, -90, 90; crs = (src = source, dest = dest,));
    coastlines!(earthscene; crs = (src = source, dest = dest,));

    colorbar = layout[1, 2] = LColorbar(scene, sf, width = 20);
    colsize!(layout, 1, Relative(1))
    rowsize!(layout, 1, Aspect(1, aspect_ratio))
    tt = layout[0, :] = LText(scene, titletext, textsize = 22);

    return scene, sf, tt
end

scene, sf, tt = plotfield(Amean; titletext = titletext, cmap = cmap)
Makie.save("testmakie.png", scene)

# %% Then, make animation
scene, sf, tt = plotfield(A[:, :, 1], total_crange; titletext = titletext, cmap=cmap)
record(scene, "anim_makie.mp4", 1:size(A, 3); framerate = 10) do i
    sf.color = colorfunc(i)
    tt.attributes.text[] = texttime(i)
end
