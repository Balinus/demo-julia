using ClimateTools
cd("/media/DATA1TB/CMIP5")

file = "/media/DATA1TB/CMIP5/tasmax_day_CanESM2_rcp45_r1i1p1_20060101-21001231.nc"
vari = "tasmax"

shapefile = "/home/proy/.julia/dev/ClimateTools/test/data/SudQC_GCM.shp"

P = extractpoly(shapefile,n=1)

B = load(file, "tasmax", poly = P, start_date=(2006,01,01), end_date=(2006, 01, 01, 23))

A = load(file, "tasmax", start_date=(2006,01,01), end_date=(2006, 01, 31, 23))

interp = regrid(A, B)#, method="cubic")
