
function build_params_coords(lon_params, lat_params)
    points_params =  Array{LatLon}(undef, length(lon_params[:]))

    for I in eachindex(lon_params)

        points_params[I] = LatLon(lat_params[I], lon_params[I])

    end

    return points_params
end



function mindist_idx(point, points_params)

    R = Array{Float64}(undef, length(points_params))

    for r in eachindex(points_params)
        R[r] = distance(point, points_params[r])
    end

    val, idx = findmin(R)

    return idx

end
