using ClimateTools, AxisArrays
using Shapefile, Glob
using Extremes
using CSV
using Interpolations
using ArgCheck
using Dates
using PyPlot

function findos()
    direc = glob("*", "/home/proy")

    idx = findall(f, direc)

    if !isempty(idx)
        return "home"
    else
        return "ouranos"

    end

end

f(x) = x == "/home/proy/venvs"

gevfile = "/home/proy/GitRepos/demo-julia/data/GEV_24h_pcp_interpolated_v2.csv"

# GEV PARAMETERS
gevparams = CSV.read(gevfile)

if findos() == "ouranos"

    repout = "/home/proy/NEREE_root/exec/proy/DATA/GEV/"
    canesm2 = "/home/proy/NEREE_root/dmf2/scenario/external_data/CMIP5/CCCMA/CanESM2/rcp85/day/atmos/r3i1p1/pr/pr_day_CanESM2_rcp85_r3i1p1_20060101-21001231.nc"
    repData = "/home/proy/SCEN3_root/scen2/scenario/migration_netcdf/nrcan/nrcan_canada_daily/"
    # obs_file_shp = "/home/proy/DORIS/Shapefile/TMP/TmpforPontos/cgb_lgc_canada_shp_fr/prov_la_p_geo83_f.shp"

elseif findos() == "home"

    repout = "/media/DATA1TB/DATA/GEV/"
    canesm2 = "/media/DATA1TB/CMIP5/pr_day_CanESM2_rcp85_r1i1p1_20060101-21001231.nc"
    repData = "/media/DATA1TB/CMIP5/pr_day_CanESM2_rcp85_r1i1p1_20060101-21001231.nc"
    # obs_file_shp = "/home/proy/DORIS/Shapefile/TMP/TmpforPontos/cgb_lgc_canada_shp_fr/prov_la_p_geo83_f.shp"

end


# ==================
# CanESM2


poly_gcm = [[NaN -52 -84 -84 -52 -52];[NaN 35 35 65 65 35]]
poly_obs = [[NaN -62 -76 -76 -62 -62];[NaN 50 50 55 55 50]]

CanESM2 = load(canesm2, "pr", poly = poly_gcm, data_units = "mm")

ref = temporalsubset(CanESM2, (2006, 01, 01), (2030, 12, 31, 23))
fut = temporalsubset(CanESM2, (2031, 01, 01), (2100, 12, 31, 23))


# ==================
# Observations

# nrcan_pr = glob("nrcan*pr*", repData)
# nrcan_pr = nrcan_pr[12:41] # do only 1961-1990

# # Region administratives
# obs_shp = open(obs_file_shp) do fd
#     read(fd, Shapefile.Handle)
# end

# poly_obs = shapefile_coords_poly(obs_shp.shapes[498])

obs = load(canesm2, "pr", poly = poly_obs, data_units = "mm", start_date = (2006, 01, 01), end_date = (2030,12,31,23))

ref = regrid(ref, obs)
fut = regrid(fut, obs)

ref = ref * 1.1


D = biascorrect_extremes(obs, ref, fut, gevparams=gevparams, method="multiplicative")
Dqqmap = qqmap(obs, ref, fut, method="multiplicative")

# Figure
plot(annualmax(D) - annualmax(Dqqmap))
mapclimgrid(annualmax(D) - annualmax(Dqqmap), region = "Quebec")
figure()
plot(annualmax(D), label="ext")
plot(annualmax(Dqqmap), label="qqmap")
plot(annualmax(fut), label="fut")
# write(CanESM2_regrid, string(repout, "CanESM2_regrid.nc"))
